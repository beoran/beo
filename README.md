# beo

Beo is open source system programming language that makes it easier to build 
reliable, and efficient software. Currently in design phase.

# Introduction

Do you kniow Ancient Roman buildings, still standing after 2000 years? 
Solidly built, with thick walls, using simple shapes, elegantly decorated or 
not, but all in all, built to last. Legend has it that ancient Roman engineers 
had to stand under any bridge they designed as a Roman legion marched over it. 
Their design principle was robustness though simplicity and overenigeering, 
whilst keeping practical use and building costs in mind.

In the programming world, though, such robustness though simplicity is hard to 
find, especially for low level programming languages. Enter Beo. 

## Design

### Guiding Principles

The Beo programming language design will be guided by the following principles, 
in order of importance. The language and the software created with it should 
be: 

1. Robust. Software should run correctly, and be easy to maintain.
2. Simple. Do the simplest things that can work.
3. Overengineered. Trade space and time for robustness or simplicity if needed.
4. Practical. Software should be practical and easy to use.
5. Economical. Software should use a little resources as possible. This 
   includes performance and memory use, also during compilation.

### Syntax

First of all to start with the least consequential but most heated topic of 
language design: the syntax of the laguage. There, keeping the design 
principles in mind, one important thing needs to be remarked: while syntax is 
arbitrary, the *cost* of parsing syntaxes is not. It is faster and uses less 
resources to keep the syntax simple, so an LL(1) or similar parser can parse it 
quickly. 

Some languages, like C++ require a complex GRL parser, so even theoretically
compilation of such a language cannot be fast. At the time of this writing, 
the systems programming language that best meets this criterion is Go language. 
Hence, Go language will be used as the basis of the syntax of the Beo language.

### Compilation

Beo is a compiled language since this the way to robustness. Moreso,
if something can be done at compile time, it should be done at compile time.

To compile Beo source code into an executable or a library, the compiler needs 
to be able to find not only the sources to compile, but also dependencies, etc.
In some other languages, this is either platform specific, requiring extra 
tools such as Makefiles, Other languages have specific files to arrage this, 
such as go.mdo or crate files. 

Beo works according to the principle of convention over configuration. 
While configuration is OK when structly needed, otherwise conventions are 
used. 

In Beo, the compiler works on the whole of a "project". A project is 
a folder with a project.beo file in it with compile time instructsions.
A project may have can have several packages (=libraries) in it, 
which are subsirectories, or sub-sub directories of the project folder, 
and which may have an arbitrary name, except for out, cmd, lib, bin, or res.

A package may optionally have a package.beo file with 
compile time instructions.

A project may have one or more executables. The source code for 
executables must be placed in a cmd/<exe_name>.
Futhermore there are some special directories in every project that cannot 
be used as package names:

* The lib folder contains compiled libraries.
* The bin folder contains compiled binaries.
* The res folder contains sources of resources.
* The out folder contains build output.
 

In Beo compile time instructions are part of the language. However, these 
are explicitly NOT Turing complete. Compile time instruction in Beo start
with a #, reminiscent of the C preprocessor, but unlike a preprocessor, 
this is integrated in the language. A block of compile time statements begins
with #( and ends with #), and does not nest. Some compile time statements 
are only allowed at the top of the compilation unit, or in the project.beo
or package.beo file. 

The compile time language of beo is a limited form of beo, which 
only supports function calls to builtin compile time functions, together 
with boolean, numeric and string constants, constant definitions, and 
basic operators on them.

The following compile time constants are available:

* OS: the operating system targeted by the Beo compiler.
* ARCHITECTURE: the architecture targeted by the Beo compiler.
* VARIANT: the sub-architecture targeted by the Beo compiler.
* TEST: whether or not the program is running in test mode.

The following compile time functions are supported: 

* procect("name"): The name of the project. Needs to be
* version("<version of language>"): The project requires Beo
  language of the given version. Versions are strings but follow semver. 
* require("<dependency>", "<version of dependency>"): The project requires 
  the given version of the given dependency.
* build_if(bool): Only build this file if bool is true.
* generate("generator command line"): Generator command line is invoked when when beo generate is called.


## Memory management

In this context people talk about stack and heap, but these are implementation
details of the currently popular CPU and OS. If we have a function based language,
where functions call each other in a call-stack wise fashion. Multithreading 
compicates this, though as suddenly different parts of the program may be 
accessing different memory locations, BUT, the thread is started at a certain point, 
so it should only acccess memory from it's caller or itself. The exception to that 
are exported global variables which can be used everywhere.

Allocation can happen when resizing slices or when allocating a single struct, string or other object.

name | caller allocations | callee allocations | callee returns | use of arguments | how to free correctly
borrowing function | allocates memory   | none | none | read only  | no leak if caller frees memory before returning.
nonallocating mutator | allocates memory   | none | none | writes to that memory, but does not store any pointers | no leak if caller frees memory before returning
allocating mutator | allocates memory   | allocates memory | none | stores reference of allocation | no leak if caller (or recursively one of it's callers) frees memory, including the allocation the callee made
allocator function | does not allocate  | allocates memory | return reference of allocation | read only | no leak if caller (or recursively one of it's callers) frees memory allocation the callee made


The compliation to this is that though mutations, a web of non-hierarchical
references can be formed exist, for example, for parse nodes or linked lists.
In languages such as C, C++, or pascal, such references can become dangling
references if the referred to object is deallocated. 


This situation resembles a Unix file system with a single tree, 
and with hard and symbolic links. On a Unix file system, unlike on,
say, DOS, a file name does not directly refer to a disk block, 
but to an i-node, which in turn points to the disk block.
Hard links are reference counted, and a file is only
truly deleted if no more hard links are pointing at it. 
Furthermore there are soft links with do not prevent 
deletion, but which are not avlid anumore of a like is deleted.

## Implementation

### Bootstrapping and back ends

Beo will be bootstrapped from Go, suing no dependencies apart from the 
Go standard library. It will have a C back end with a small C run time
based on the C standard library to begin with, probably adding a fasm back 
end as well. Later and assembler and linker might become part of Beo if that 
is useful.

