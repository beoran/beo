/*
This is the grammar for LL1 itself. The lexer is the Go language scanner,
so comments are ignored.
*/

Grammar -> Rules `
type Position = scanner.Position
type Ll1Token struct {
	Position
	Ll1TokenType
	Value string
}
` .
Rules -> Rule OptRules .
OptRules -> '.' Rules | epsilon.
Rule -> Name arrow Definition Template.
Name -> identifier .
Template -> rawString | epsilon .
// Alternates consist of sequences.
Definition -> Alternates . 
Alternates -> Sequence OptSequences .
OptSequences -> '|' Alternates | epsilon.
Sequence -> Element OptElements . 
OptElements -> Element OptElements | epsilon.
Element -> Parenthesis .
Element -> Name .
Element -> literal /*| Rule */.
Parenthesis -> '(' Definition ')' .
// Lexer specifications. These are rules that have a name that 
// begins with a lower case letter
dot          -> '.' .
or           -> '|' .
identifier   -> ruleName | terminalName .
ruleName     -> "re:[[:isUpper:]][[:isAlNum]]" . 
terminalName -> "re:[[:isLower:]][[:isAlNum]]" .
epsilon      -> "epsilon" | 'ε' . 
arrow        -> "->" | '→' .
literal -> stringLiteral | charLiteral .
stringLiteral 	-> "re:\"[^\"]+\"" .
charLiteral 	-> "re:'[^']+'" .
rawString 		-> "re:`[^`]+`/" .
whiteSpace 		-> "re:[:isblank:]+" .
lineComment 	-> "re://[^\n]\n" .
handCoded		-> "code:" `
// Not implemented
` .

/*
Grammar			                                  identifier			   ∅										no	yes
Rules			                                  identifier			   ∅										no	yes
OptRules		                                  .						   ∅										yes	yes
Rule			                                  identifier			  .										no	yes
Name			                                  identifier			  ) literal ( identifier | arrow .		no	yes
Definition		                                  literal ( identifier	  ) .									no	yes
Alternates		                                  literal ( identifier	  ) .									no	yes
OptSequences	                                  |						  ) .									yes	yes
Sequence		                                  literal ( identifier	  ) | .									no	yes
OptElements		                                  literal ( identifier	  ) | .									yes	yes
Element			                                  literal ( identifier	  ) literal ( identifier | .			no	yes
Parenthesis		                                  (						  ) literal ( identifier | .			no	yes


Output of:
./ll1 ll1.ll1 -t ll1.template.debug | column -e -s '~' -t -x

Rule               Definition                      First                   Follow
----------------  ----------------                ----------------        ----------------
Grammar           (Rules)                         identifier              $
Rules             (Rule   OptRules)               identifier              $
OptRules          ("."   Rules | ε)               . ε                     $
Rule              (Name   arrow   Definition)     identifier              . ε
Name              (identifier)                    identifier              ( arrow identifier literal ε
Definition        (Alternates)                    ( identifier literal    ) . ε
Alternates        (Sequence   OptSequences)       ( identifier literal    ) . ε
OptSequences      ("|"   Alternates | ε)          | ε                     ) . ε
Sequence          (Element   OptElements)         ( identifier literal    | ε
OptElements       (Element   OptElements | ε)     ( identifier literal ε  | ε
Element           (Parenthesis | Name | literal)  ( identifier literal    ( identifier literal ε
Parenthesis       ("("   Definition   ")")        (                       ( identifier literal ε



*/
