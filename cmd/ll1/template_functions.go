package main

import (
	"fmt"
	"regexp"
	"strings"
	"text/template"
    "time"
)


func templateNewMap(args ...interface{}) (interface{}, error) {
	result := map[string]interface{}{}
	for i := 1; i < len(args); i += 2 {
		key, ok := args[i-1].(string)
		if !ok {
			return nil, fmt.Errorf("Map: key %v must be string.", key)
		}
		result[key] = args[i]
	}

	return result, nil
}

func templateNewList(args ...interface{}) interface{} {
	return args
}


func templateToString(v interface{}) string {
	return fmt.Sprintf("%s", v)
}

func templateCompileRegexp(reAny interface{}) (interface {}, error) {
    reStr, ok := reAny.(string)
    if !ok {
			return nil, fmt.Errorf("CompileRegexp: %v must be string.", reAny)
	}
    re, err := regexp.Compile(reStr)
    return re, err
}


/* 
func (re *Regexp) ExpandString(dst []byte, template string, src string, match []int) []byte
    ExpandString is like Expand but the template and source are strings. It
    appends to and returns a byte slice in order to give the calling code
    control over allocation.

func (re *Regexp) FindAllString(s string, n int) []string
    FindAllString is the 'All' version of FindString; it returns a slice of all
    successive matches of the expression, as defined by the 'All' description in
    the package comment. A return value of nil indicates no match.

func (re *Regexp) FindAllStringIndex(s string, n int) [][]int
    FindAllStringIndex is the 'All' version of FindStringIndex; it returns a
    slice of all successive matches of the expression, as defined by the 'All'
    description in the package comment. A return value of nil indicates no
    match.

func (re *Regexp) FindAllStringSubmatch(s string, n int) [][]string
    FindAllStringSubmatch is the 'All' version of FindStringSubmatch; it returns
    a slice of all successive matches of the expression, as defined by the 'All'
    description in the package comment. A return value of nil indicates no
    match.

func (re *Regexp) FindAllStringSubmatchIndex(s string, n int) [][]int
    FindAllStringSubmatchIndex is the 'All' version of FindStringSubmatchIndex;
    it returns a slice of all successive matches of the expression, as defined
    by the 'All' description in the package comment. A return value of nil
    indicates no match.

func (re *Regexp) FindString(s string) string
    FindString returns a string holding the text of the leftmost match in s of
    the regular expression. If there is no match, the return value is an empty
    string, but it will also be empty if the regular expression successfully
    matches an empty string. Use FindStringIndex or FindStringSubmatch if it is
    necessary to distinguish these cases.

func (re *Regexp) FindStringIndex(s string) (loc []int)
    FindStringIndex returns a two-element slice of integers defining the
    location of the leftmost match in s of the regular expression. The match
    itself is at s[loc[0]:loc[1]]. A return value of nil indicates no match.

func (re *Regexp) FindStringSubmatch(s string) []string
    FindStringSubmatch returns a slice of strings holding the text of the
    leftmost match of the regular expression in s and the matches, if any, of
    its subexpressions, as defined by the 'Submatch' description in the package
    comment. A return value of nil indicates no match.

func (re *Regexp) FindStringSubmatchIndex(s string) []int
    FindStringSubmatchIndex returns a slice holding the index pairs identifying
    the leftmost match of the regular expression in s and the matches, if any,
    of its subexpressions, as defined by the 'Submatch' and 'Index' descriptions
    in the package comment. A return value of nil indicates no match.

func (re *Regexp) LiteralPrefix() (prefix string, complete bool)
    LiteralPrefix returns a literal string that must begin any match of the
    regular expression re. It returns the boolean true if the literal string
    comprises the entire regular expression.
    
func (re *Regexp) MatchString(s string) bool
    MatchString reports whether the string s contains any match of the regular
    expression re.

func (re *Regexp) FindString(s string) string
    FindString returns a string holding the text of the leftmost match in s of
    the regular expression. If there is no match, the return value is an empty
    string, but it will also be empty if the regular expression successfully
    matches an empty string. Use FindStringIndex or FindStringSubmatch if it is
    necessary to distinguish these cases.

func (re *Regexp) FindStringIndex(s string) (loc []int)
    FindStringIndex returns a two-element slice of integers defining the
    location of the leftmost match in s of the regular expression. The match
    itself is at s[loc[0]:loc[1]]. A return value of nil indicates no match.
    
 func (re *Regexp) Split(s string, n int) []string
    Split slices s into substrings separated by the expression and returns a
    slice of the substrings between those expression matches.

    The slice returned by this method consists of all the substrings of s not
    contained in the slice returned by FindAllString. When called on an
    expression that contains no metacharacters, it is equivalent to
    strings.SplitN.

    Example:

        s := regexp.MustCompile("a*").Split("abaabaccadaaae", 5)
        // s: ["", "b", "b", "c", "cadaaae"]

    The count determines the number of substrings to return:

        n > 0: at most n substrings; the last substring will be the unsplit remainder.
        n == 0: the result is nil (zero substrings)
        n < 0: all substrings

func (re *Regexp) String() string
    String returns the source text used to compile the regular expression.

func (re *Regexp) SubexpNames() []string
    SubexpNames returns the names of the parenthesized subexpressions in this
    Regexp. The name for the first sub-expression is names[1], so that if m is a
    match slice, the name for m[i] is SubexpNames()[i]. Since the Regexp as a
    whole cannot be named, names[0] is always the empty string. The slice should
    not be modified.

*/

func iadd(i1, i2 int)  int {
    return  i1 + i2
}

func isub(i1, i2 int)  int {
    return  i1 - i2
}

func imul(i1, i2 int)  int {
    return  i1 * i2
}

func idiv(i1, i2 int)  int {
    return  i1 / i2
}

func fadd(i1, i2 float64)  float64 {
    return  i1 + i2
}

func fsub(i1, i2 float64)  float64 {
    return  i1 - i2
}

func fmul(i1, i2 float64)  float64 {
    return  i1 * i2
}

func fdiv(i1, i2 float64)  float64 {
    return  i1 / i2
}

    

var templateFunctionMap template.FuncMap = template.FuncMap{
	"CompileRegexp":        templateCompileRegexp,
    "Compare":              strings.Compare,
	"Contains":             strings.Contains,
	"ContainsAny":          strings.ContainsAny,
	"ContainsRune":         strings.ContainsRune,
	"Count":                strings.Count,
	"EqualFold":            strings.EqualFold,
	"Fields":               strings.Fields,
	"FieldsFunc":           strings.FieldsFunc,
	"HasPrefix":            strings.HasPrefix,
	"HasSuffix":            strings.HasSuffix,
	"Index":                strings.Index,
	"IndexAny":             strings.IndexAny,
	"IndexByte":            strings.IndexByte,
	"IndexFunc":            strings.IndexFunc,
	"IndexRune":            strings.IndexRune,
	"Join":                 strings.Join,
	"LastIndex":            strings.LastIndex,
	"LastIndexAny":         strings.LastIndexAny,
	"LastIndexByte":        strings.LastIndexByte,
	"LastIndexFunc":        strings.LastIndexFunc,
	"Map":                  strings.Map,    
	"MatchString":          regexp.MatchString,
	"NewMap":               templateNewMap,
	"NewList":              templateNewList,
    "Now":                  time.Now,
	"Repeat":               strings.Repeat,
	"Replace":              strings.Replace,
	"ReplaceAll":           strings.ReplaceAll,
	"Split":                strings.Split,
	"SplitAfter":           strings.SplitAfter,
	"SplitAfterN":          strings.SplitAfterN,
	"SplitN":               strings.SplitN,
	"Title":                strings.Title,
	"ToLower":              strings.ToLower,
	"ToLowerSpecial":       strings.ToLowerSpecial,
	"ToString":             templateToString,
	"ToTitle":              strings.ToTitle,
	"ToTitleSpecial":       strings.ToTitleSpecial,
	"ToUpper":              strings.ToUpper,
	"ToUpperSpecial":       strings.ToUpperSpecial,
	"Trim":                 strings.Trim,
	"TrimFunc":             strings.TrimFunc,
	"TrimLeft":             strings.TrimLeft,
	"TrimLeftFunc":         strings.TrimLeftFunc,
	"TrimPrefix":           strings.TrimPrefix,
	"TrimRight":            strings.TrimRight,
	"TrimRightFunc":        strings.TrimRightFunc,
	"TrimSpace":            strings.TrimSpace,
	"TrimSuffix":           strings.TrimSuffix,
    "iadd":                 iadd,
    "isub":                 isub,
    "imul":                 imul,
    "idiv":                 idiv,
    "fadd":                 fadd,
    "fsub":                 fsub,
    "fmul":                 fmul,
    "fdiv":                 fdiv,
}


