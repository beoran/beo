{{- /* This template generates a recursive descent parser based on the */ -}}
{{- /* information about the LL(1) grammar processed by the ll1 tool.  */ -}}

/* 
 * {{.OutName}}: Parser for the {{.Grammar.Top.Name}} grammar. 
 * Generated by the ll1 tool from {{.InName}} at {{Now}}.
 * Based on template: {{.Templates}}
 * 
 * Available definitions: 
 * {{.Definitions}}
 * 
 * DO NOT EDIT. 
 */
package {{ .Package }}

{{ range .Import }}
import "{{.}}"
{{ end }}

{{$prefix := .Prefix }}

{{- $Parser := ( printf "%s%s" $prefix "Parser") -}}
{{- $Lexer := ( printf "%s%s" $prefix "Lexer") -}}
{{- $TokenType := ( printf "%s%s" $prefix "TokenType") -}}

type {{$Parser}} struct {
}

type {{$Lexer}} struct {
    {{.LexerType}}
}

type {{$TokenType}} rune

{{ $tokenTypeValue := 2 }}

{{ range .Grammar.Rules -}}
{{- $ruleName := .Name -}}
{{ if .Template }}
// Expanded from template of rule {{$ruleName}}
{{ .Template }}
{{ end }}
{{- $terminal := .IsTerminal -}}
{{- if $terminal -}}
{{- $TokenTypeName := ( printf "%s%s" $TokenType $ruleName) -}}
const {{$TokenTypeName}} {{$TokenType}} = {{$TokenType}}(-{{$tokenTypeValue}})
{{ $tokenTypeValue = (iadd $tokenTypeValue 1) }}
func ( *{{$Lexer}}) Lex{{$TokenTypeName}}() ({{$TokenType}}, error) {
    result := {{$TokenTypeName}}
    return result, nil
}

{{ else }}
{{ $RuleType := ( printf "%s%s" $prefix $ruleName) }}
type {{$RuleType}} struct {    
}

func ( *{{$Parser}}) Parse{{$RuleType}}() ({{$RuleType}}, error) {
    result := {{$RuleType}} {}
    return result, nil 
}
{{end}}

{{ end }}

