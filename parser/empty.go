package parser

// EmptyValue is a Muesli value without contents
type EmptyValue struct {
}

// EmptyType is the type of !nil values
const EmptyType  		= TypeValue("Empty")

// NilValue is the value of !nil in Muesli
var NilValue = Value(nil)


func (val EmptyValue) String() string {
	return "<empty>"
}
	
func (v EmptyValue) Type() TypeValue  { return EmptyType }


func (from EmptyValue) Convert(to interface{}) error {
	return NewErrorValuef("Cannot convert the empty value %v to %v", from, to)
}

