package parser

import (
	_ "strings"
	"testing"
)

func HelperFailOnErrors(ast * Ast, expected int, test *testing.T) {
	if ast == nil { 
		test.Errorf("Parse failed, AST is nil.")
		return
	}	
	if ast.IsNone() {
		test.Logf("Parse failed, %d parse errors expected", expected)
	}
	errors := ast.Errors()
	if len(errors) != expected {
		test.Log("Parse errors:\n")
		for _, err := range errors {
			test.Logf("%s\n", err.String())
		}
		test.Errorf("There were %d parse errors, %d expected", len(errors), expected)
	}
}

func HelperParseAndFailOnErrors(prog string, expected int,
	parsefunc func(*Parser) *Ast, test *testing.T) {
	/*	defer func(){
			err := recover()
			if err != nil {
				test.Errorf("Parse error: %s", err)
			}
		}()
	*/
	
	parser := NewParserFromString(prog)
	parser.SetLogger(&testLogger{"", 0, test})
	ast := parsefunc(parser)
	HelperFailOnErrors(ast, expected, test)
	if ast != nil { 
		test.Logf("AST OK")
		test.Logf("\nAst: %s\n",  ast.Dump())		
	}
}

func TestParser1(test *testing.T) {
	com := `puts "hello" 
	say ( add 5 10 ) .`
	parser := NewParserFromString(com)
	ast := parser.Parse()
	if ast != nil {
		test.Logf("AST OK") 
		// ast.Display()
	}	
}

func TestParser2(test *testing.T) {
	com := `puts "hello"
	say ( add 5 10 ) .`
	HelperParseAndFailOnErrors(com, 0, (*Parser).Parse, test)
}

func TestParenthesis(test *testing.T) {
	HelperParseAndFailOnErrors(`( add 5 10 ) `, 0, (*Parser).Parse, test)
	// HelperParseAndFailOnErrors(`( add 5 10 ) `, 0, (*Parser).ParseParenthesis, test)
	// HelperParseAndFailOnErrors(`add 5 10 `, 0, (*Parser).ParseCommand, test)
}

func TestSubstitute(test *testing.T) {
	HelperParseAndFailOnErrors(`( add 5 10 ) `, 0, (*Parser).Parse, test)
	// HelperParseAndFailOnErrors(`( add 5 10 ) `, 0, (*Parser).ParseParenthesis, test)
	// HelperParseAndFailOnErrors(`add 5 10 `, 0, (*Parser).ParseCommand, test)
}

func TestSubstituteWithError(test *testing.T) {
	HelperParseAndFailOnErrors(`( add 5 10 ) ) `, 1, (*Parser).Parse, test)
	// HelperParseAndFailOnErrors(`( add 5 10 ) `, 0, (*Parser).ParseParenthesis, test)
	// HelperParseAndFailOnErrors(`add 5 10 `, 0, (*Parser).ParseCommand, test)
}


func TestParseDesignFile(test *testing.T) {
    
	parser, err := NewParserFromFilename("design_muesli.muesli")
	if err != nil { 
        test.Errorf("Error parsingfile : %s", err)
        return
    } 
    parser.SetLogger(&testLogger{"", 0, test})
    ast := parser.Parse()
    if ast != nil { 
		test.Logf("Ast OK.")
		test.Logf("\nAst: %s\n",  ast.Dump())
	}
    
}

