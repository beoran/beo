package parser

import "fmt"

type Keyword struct {
	Name string
	TokenKind
	Value
}

var Keywords []*Keyword = []*Keyword{
    &Keyword{"break"     ,    TokenKindKeywordBreak     ,        NilValue},
    &Keyword{"case"      ,    TokenKindKeywordCase      ,        NilValue},
    &Keyword{"const"     ,    TokenKindKeywordConst     ,        NilValue},
    &Keyword{"continue"  ,    TokenKindKeywordContinue  ,        NilValue},
    &Keyword{"default"   ,    TokenKindKeywordDefault   ,        NilValue},
    &Keyword{"defer"     ,    TokenKindKeywordDefer     ,        NilValue},
    &Keyword{"else"      ,    TokenKindKeywordElse      ,        NilValue},
    &Keyword{"fallthough",    TokenKindKeywordFallthough,        NilValue},
    &Keyword{"for"       ,    TokenKindKeywordFor       ,        NilValue},
    &Keyword{"func"      ,    TokenKindKeywordFunc      ,        NilValue},
    &Keyword{"goto"      ,    TokenKindKeywordGoto      ,        NilValue},
    &Keyword{"if"        ,    TokenKindKeywordIf        ,        NilValue},
    &Keyword{"import"    ,    TokenKindKeywordImport    ,        NilValue},
    &Keyword{"interface" ,    TokenKindKeywordInterface ,        NilValue},
    &Keyword{"map"       ,    TokenKindKeywordMap       ,        NilValue},
    &Keyword{"package"   ,    TokenKindKeywordPackage   ,        NilValue},
    &Keyword{"range"     ,    TokenKindKeywordRange     ,        NilValue},
    &Keyword{"return"    ,    TokenKindKeywordReturn    ,        NilValue},
    &Keyword{"select"    ,    TokenKindKeywordSelect    ,        NilValue},
    &Keyword{"struct"    ,    TokenKindKeywordStruct    ,        NilValue},
    &Keyword{"switch"    ,    TokenKindKeywordSwitch    ,        NilValue},
    &Keyword{"type"      ,    TokenKindKeywordType      ,        NilValue},
    &Keyword{"var"       ,    TokenKindKeywordVar       ,        NilValue},
}
    

/*


*/


var _ Value = &Keyword{}

const KeywordType = TypeValue("Keyword")

func (kw * Keyword) String() string {
	return fmt.Sprintf("Keyword: %s %v %v", kw.Name, kw.TokenKind, kw.Value)
}

func (*Keyword) Type() TypeValue {
	return KeywordType
}

func (from * Keyword) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case **Keyword:
			(*toPtr) = from
		case *Keyword:
			(*toPtr) = *from
		case *Value:
			(*toPtr) = from
		default:
			return NewErrorValuef("Cannot convert Keyword value %v to %v", from, to)
	}
	return nil
}

