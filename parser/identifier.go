package parser

// Identifier is either a constant, a variable , a type, or a function,
// found inside a package.

type IdentifierType int 

const (
    IdentifierTypeNone      IdentifierType = IdentifierType(0)
    IdentifierTypeConstant  IdentifierType = IdentifierType(1)
    IdentifierTypeFunction  IdentifierType = IdentifierType(2)
    IdentifierTypeType      IdentifierType = IdentifierType(3)
    IdentifierTypeVariable  IdentifierType = IdentifierType(4)
)

type Identifier interface {
    // nil package if for builtin identifiers
    Package() *Package
    Name() string
}

// This type helps implementing the identifier interface
type identifier struct {
    // the name of the identifier
    name string
    // package the identifier is in
    pack *Package
}

func (i identifier) Name() string {
    return i.name
}

func (i identifier) Package() *Package {
    return i.pack
}

