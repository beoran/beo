package parser

import "fmt"

// ErrorValue are Muesli errors
type ErrorValue struct {
	error
}

func (v ErrorValue) Type() TypeValue  { return ErrorType }

func (val ErrorValue) String() string {
	return fmt.Sprintf("%s", val.Error())
}

const ErrorType  		= TypeValue("Error")


func NewErrorValuef(format string, args ...interface{}) ErrorValue {
	err := fmt.Errorf(format, args...)
	return ErrorValue{err}
}





func (from ErrorValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *error:
			(*toPtr) = from.error
		case *ErrorValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from			
		default:
			return NewErrorValuef("Cannot convert value %v to %v", from, to)
	}
	return nil
}

