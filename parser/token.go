package parser

import (
	"fmt"
)

/* Position of a token in an input stream */
type Position struct {
	FileName string
	Line     int
	Column   int
}

func (p Position) String() string {
	return fmt.Sprintf("%s:%d:%d", p.FileName, p.Line, p.Column)
}

/* Token Kind. Uses rune since that is easier for some uses.
 */
type TokenKind rune




const (
	TokenKindNone               = TokenKind(0)
    
    // Short token kinds are simply the character that they represent in 
    // the source code, for ease of parsing
    TokenKindSelector           = TokenKind('.')
    TokenKindComma              = TokenKind(',')
    TokenKindEOS                = TokenKind(';')
    TokenKindEOX                = TokenKind('\n')
	TokenKindOpenBlock          = TokenKind('{')
	TokenKindCloseBlock         = TokenKind('}')
	TokenKindOpenList           = TokenKind('[')
	TokenKindCloseList          = TokenKind(']')
	TokenKindOpenParen          = TokenKind('(')
	TokenKindCloseParen         = TokenKind(')')
    TokenKindOperatorPlus       = TokenKind('+')
    TokenKindOperatorMinus      = TokenKind('-')     
    TokenKindOperatorStar       = TokenKind('*')
    TokenKindOperatorPercent    = TokenKind('%')
    TokenKindOperatorBinaryAnd  = TokenKind('&')     
    TokenKindOperatorBinaryOr   = TokenKind('|')     
    TokenKindOperatorRoof       = TokenKind('^')
    TokenKindOperatorLogicalNot = TokenKind('!')
    TokenKindOperatorAssign     = TokenKind('=')	
	TokenKindEOF                = TokenKind(0x255)
    longTokenKind               = TokenKind(256+iota)
    
    TokenKindIdentifier
	TokenKindIntegerLiteral
	TokenKindFloatLiteral
    TokenKindRuneLiteral
	TokenKindStringLiteral
    TokenKindBooleanLiteral
       
    TokenKindOperatorShiftLeft       
    TokenKindOperatorShiftRight      
    TokenKindOperatorBinaryXand      
    TokenKindOperatorAssignPlus      
    TokenKindOperatorAssignMinus     
    TokenKindOperatorAssignStar      
    TokenKindOperatorAssignBinaryAnd 
    TokenKindOperatorAssignBinaryOr  
    TokenKindOperatorAssignBinaryXor 
    TokenKindOperatorAssignShiftLeft 
    TokenKindOperatorAssignShiftRight
    TokenKindOperatorAssignBinaryXand
    TokenKindOperatorLogicalAnd      
    TokenKindOperatorLogicalOr       
    TokenKindOperatorSend            
    TokenKindOperatorIncrement       
    TokenKindOperatorDecrement       
    TokenKindOperatorEquals          
    TokenKindOperatorLessThan        
    TokenKindOperatorMoreThan        
    
    
    TokenKindOperatorLogicalNotEqual 
    TokenKindOperatorLessThanOrEqual 
    TokenKindOperatorMoreThanOrEqual 
    TokenKindOperatorVarArg          
            
    TokenKindKeywordBreak
    TokenKindKeywordCase
    TokenKindKeywordConst
    TokenKindKeywordContinue
    TokenKindKeywordDefault
    TokenKindKeywordDefer
    TokenKindKeywordElse
    TokenKindKeywordFallthough
    TokenKindKeywordFor
    TokenKindKeywordFunc
    TokenKindKeywordGoto
    TokenKindKeywordIf
    TokenKindKeywordImport
    TokenKindKeywordInterface
    TokenKindKeywordMap
    TokenKindKeywordPackage
    TokenKindKeywordRange
    TokenKindKeywordReturn
    TokenKindKeywordSelect
    TokenKindKeywordStruct
    TokenKindKeywordSwitch
    TokenKindKeywordType
    TokenKindKeywordVar
    TokenKindNilLiteral
    TokenKindType
    TokenKindError
)

/* Names of the different token types. */
var TokenKindNames map[TokenKind]string = map[TokenKind]string{
	TokenKindNone:       "None",
	/*
    TokenKindInteger:    "Integer",
	TokenKindFloat:      "Float",
	TokenKindSymbol:     "Symbol",
	TokenKindString:     "String",
    */ 
	TokenKindBooleanLiteral:    "Boolean",
    /*
    TokenKindNil:        "Nil",
	TokenKindWord:       "Word",
	TokenKindType:       "Type",
	TokenKindOperator:   "Operator",   
    */
	TokenKindSelector:   "Selector",
	TokenKindOpenBlock:  "OpenBlock",
	TokenKindCloseBlock: "CloseBlock",
	TokenKindOpenList:   "OpenList",
	TokenKindCloseList:  "CloseList",
	TokenKindOpenParen:  "OpenParen",
	TokenKindCloseParen: "CloseParen",
	TokenKindError:      "Error",
	TokenKindEOX:        "EOX",
	TokenKindEOF:        "EOF",
}

/* Reverse map of the names of the different token types. */
var NamesTokenKind map[string] TokenKind = map[string]TokenKind{}

func init () {
	for k, v := range TokenKindNames {
		NamesTokenKind[v] = k
	}
}

/* Transforms a token kind to a String */
func (kind TokenKind) String() string {
	name, ok := TokenKindNames[kind]
	if !ok {
		return "Unknown TokenKind!"
	}
	return name
}

type Token struct {
	TokenKind
	Value
	Position
}

func (token Token) String() string {
	if token.Value == nil {
		return fmt.Sprintf("%s:%s:nil", token.Position.String(), token.TokenKind.String())

	}
	return fmt.Sprintf("%s:%s:%v", token.Position.String(), token.TokenKind.String(), token.Value)
}

func (token Token) Error() string {
	if token.TokenKind == TokenKindError {
		return token.Value.String()
	}
	return "No error"
}

/* Returns whether or not the token is the last to be expected,
 * that is either an error or EOF. */
func (token Token) IsLast() bool {
	switch token.TokenKind {
	case TokenKindError, TokenKindEOF, TokenKindNone:
		return true
	default:
		return false
	}
}

/* Creates a new token. */
func NewToken(kind TokenKind, val Value, pos Position) Token {
	return Token{kind, val, pos}
}

/* Creates a token with type TokenTypeNone */
func NoToken() Token {
	return NewToken(TokenKindNone, NilValue, Position{})
}

func (token Token) IsNone() bool {
	return token.TokenKind == TokenKindNone
}

func (token Token) IsError() bool {
	return token.TokenKind == TokenKindError
}
