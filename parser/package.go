package parser

// Package is a package in which types are defined.
type Package struct {
    // Full name of the package
    Name string
    // Alias under which the package is used.
    Alias string
    // Known identifiers of a package, either constants, 
    // variables, functions or types.
    Identifiers map[string]Identifier
}



