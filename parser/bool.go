package parser

const BoolType   		= TypeValue("Bool")

// IntValue are Muesli booleans
type BoolValue bool

const (
	// TrueValue is the Muesli side equvalent to true
	TrueValue       = BoolValue(true)
	// FalseValue is the Muesli side equvalent to false
	FalseValue      = BoolValue(false)
)

func (val BoolValue) String() string {
	if bool(val) {
		return "true"
	} else {
		return "false"
	}
}

func (v BoolValue) Type() TypeValue   { return BoolType }


func (from BoolValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *bool:			 
			(*toPtr) = bool(from) 		
		case *BoolValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from												
		default:
			return NewErrorValuef("Cannot convert value %v to %v", from, to)
	}
	return nil
}

