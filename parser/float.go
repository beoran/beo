package parser

import "fmt"

const FloatType  		= TypeValue("Float")

// FloatValue are Muesli floating point values
type FloatValue float64

func (val FloatValue) String() string {
	return fmt.Sprintf("%f", float64(val))
}


func (v FloatValue) Type() TypeValue  { return FloatType }

func (from FloatValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *int8:
			(*toPtr) = int8(from)
		case *int16:
			(*toPtr) = int16(from)
		case *int32:
			(*toPtr) = int32(from)
		case *int64:
			(*toPtr) = int64(from)
		case *int:
			(*toPtr) = int(from)
		case *bool:			 
			(*toPtr) = (from != 0) 		
		case *float32:
			(*toPtr) = float32(from)
		case *float64:
			(*toPtr) = float64(from)
		case *FloatValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from
		default:
			return NewErrorValuef("Cannot convert FloatValue value %v to %v", from, to)
	}
	return nil
}

