package parser

const WordType   		= TypeValue("Word")
// IdentifierValue are Muesli command words
type IdentifierValue string

func (val IdentifierValue) String() string {
	return string(val)
}

func (v IdentifierValue) Type() TypeValue   { return WordType }

func (from IdentifierValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *IdentifierValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from			
		default:
			return NewErrorValuef("Cannot convert IdentifierValue %v to %v", from, to)
	}
	return nil
}
