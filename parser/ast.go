// Abstract Syntax tree for the MUESLI interpreter
package parser

import (
	"fmt"
	"strings"
)

type TestCompact uint64

const (
    Comact1 TestCompact = TestCompact('a' | ('b' << 8) | ('c' << 16) | ('d' << 24))
)


type AstBasicMetaKind string

type AstMetaKindNone AstBasicMetaKind
type AstMetaKindProgram AstBasicMetaKind
type AstMetaKindStatements AstBasicMetaKind
type AstMetaKindStatement AstBasicMetaKind
type AstMetaKindOperation AstBasicMetaKind
type AstMetaKindSelector AstBasicMetaKind
type AstMetaKindSet AstBasicMetaKind
type AstMetaKindGet AstBasicMetaKind
type AstMetaKindTarget AstBasicMetaKind
type AstMetaKindCommand AstBasicMetaKind
type AstMetaKindArguments AstBasicMetaKind
type AstMetaKindArgument AstBasicMetaKind
type AstMetaKindExpression AstBasicMetaKind
type AstMetaKindBlock AstBasicMetaKind
type AstMetaKindParenthesis AstBasicMetaKind
type AstMetaKindList AstBasicMetaKind
type AstMetaKindCapture AstBasicMetaKind
type AstMetaKindWordValue AstBasicMetaKind
type AstMetaKindWord AstBasicMetaKind
type AstMetaKindType AstBasicMetaKind
type AstMetaKindValue AstBasicMetaKind
type AstMetaKindEnd AstBasicMetaKind
type AstMetaKindError AstBasicMetaKind
type AstMetaKindFlatten AstBasicMetaKind


/* The actual types are defined as constants, the meta types are used to be 
 * able to have different behavior for them. */
const (
	AstKindNone        = AstMetaKindNone("None")
	AstKindProgram     = AstMetaKindProgram("Program")
	AstKindStatements  = AstMetaKindStatements("Statements")
	AstKindStatement   = AstMetaKindStatement("Statement")
    AstKindOperation   = AstMetaKindOperation("Operation")
    AstKindSelector    = AstMetaKindSelector("Selector")
 	AstKindSet         = AstMetaKindSet("Set")
	AstKindGet         = AstMetaKindGet("Get")
	AstKindTarget      = AstMetaKindTarget("Target")
	AstKindCommand     = AstMetaKindCommand("Command")
	AstKindArguments   = AstMetaKindArguments("Arguments")
	AstKindBlock       = AstMetaKindBlock("Block")
	AstKindParenthesis = AstMetaKindParenthesis("Parenthesis")
	AstKindList        = AstMetaKindList("List")
	AstKindWordValue   = AstMetaKindWordValue("WordValue")
	AstKindWord        = AstMetaKindWord("Word")
	AstKindType        = AstMetaKindType("Type")
	AstKindValue       = AstMetaKindValue("Value")
	AstKindEnd         = AstMetaKindEnd("End")
	AstKindError       = AstMetaKindError("Error")
    AstKindFlatten     = AstMetaKindFlatten("Flatten")
)

func (astkind AstBasicMetaKind) String() string {
	return string(astkind)
}

func (astkind AstMetaKindNone) String() string        { return "AstNone       " }
func (astkind AstMetaKindProgram) String() string     { return "AstProgram    " }
func (astkind AstMetaKindStatements) String() string  { return "AstStatements " }
func (astkind AstMetaKindStatement) String() string   { return "AstStatement  " }
func (astkind AstMetaKindOperation) String() string   { return "AstOperation  " }
func (astkind AstMetaKindSelector) String() string    { return "AstSelector   " }
func (astkind AstMetaKindSet) String() string         { return "AstSet        " }
func (astkind AstMetaKindGet) String() string         { return "AstGet        " }
func (astkind AstMetaKindTarget) String() string      { return "AstTarget     " }
func (astkind AstMetaKindCommand) String() string     { return "AstCommand    " }
func (astkind AstMetaKindArguments) String() string   { return "AstArguments  " }
func (astkind AstMetaKindArgument) String() string    { return "AstArgument   " }
func (astkind AstMetaKindExpression) String() string  { return "AstExpression " }
func (astkind AstMetaKindBlock) String() string       { return "AstBlock      " }
func (astkind AstMetaKindParenthesis) String() string { return "AstParenthesis"}
func (astkind AstMetaKindList) String() string        { return "AstList       " }
func (astkind AstMetaKindCapture) String() string     { return "AstCapture    " }
func (astkind AstMetaKindWordValue) String() string   { return "AstWordValue  " }
func (astkind AstMetaKindWord) String() string        { return "AstWord       " }
func (astkind AstMetaKindType) String() string        { return "AstType       " }
func (astkind AstMetaKindValue) String() string       { return "AstValue      " }
func (astkind AstMetaKindEnd) String() string         { return "AstEnd        " }
func (astkind AstMetaKindError) String() string       { return "AstError      " }
func (astkind AstMetaKindFlatten) String() string     { return "AstFlatten    " }

func (astkind AstMetaKindNone) IsLeaf() bool        { return false }
func (astkind AstMetaKindProgram) IsLeaf() bool     { return false }
func (astkind AstMetaKindStatements) IsLeaf() bool  { return false }
func (astkind AstMetaKindStatement) IsLeaf() bool   { return false }
func (astkind AstMetaKindOperation) IsLeaf() bool   { return false }
func (astkind AstMetaKindSelector) IsLeaf() bool    { return false }
func (astkind AstMetaKindSet) IsLeaf() bool         { return false }
func (astkind AstMetaKindGet) IsLeaf() bool         { return false }
func (astkind AstMetaKindTarget) IsLeaf() bool      { return false }
func (astkind AstMetaKindCommand) IsLeaf() bool     { return false }
func (astkind AstMetaKindArguments) IsLeaf() bool   { return false }
func (astkind AstMetaKindArgument) IsLeaf() bool    { return false }
func (astkind AstMetaKindExpression) IsLeaf() bool  { return false }
func (astkind AstMetaKindBlock) IsLeaf() bool       { return true  }
func (astkind AstMetaKindParenthesis) IsLeaf() bool { return false }
func (astkind AstMetaKindList) IsLeaf() bool        { return false }
func (astkind AstMetaKindCapture) IsLeaf() bool     { return false }
func (astkind AstMetaKindWordValue) IsLeaf() bool   { return true  }
func (astkind AstMetaKindWord) IsLeaf() bool        { return true  }
func (astkind AstMetaKindType) IsLeaf() bool        { return true  }
func (astkind AstMetaKindValue) IsLeaf() bool       { return true  }
func (astkind AstMetaKindEnd) IsLeaf() bool         { return true  }
func (astkind AstMetaKindError) IsLeaf() bool       { return true  }
func (astkind AstMetaKindFlatten) IsLeaf() bool     { return false }



type AstKind interface {
	String() string
    IsLeaf() bool
}

/*
func (Ast Ast) String() string {
	switch astkind {
	case AstMetaKindProgram:
		return "AstMetaKindProgram"
	case AstMetaKindStatements:
		return "AstMetaKindStatements"
	case AstMetaKindStatement:
		return "AstMetaKindStatement"
	case AstMetaKindSet:
		return "AstMetaKindSet"
	case AstMetaKindGet:
		return "AstMetaKindGet"
	case AstMetaKindTarget:
		return "AstMetaKindTarget"
	case AstMetaKindCommand:
		return "AstMetaKindCommand"
	case AstMetaKindArguments:
		return "AstMetaKindArguments"
	case AstMetaKindArgument:
		return "AstMetaKindArgument"
	case AstMetaKindExpression:
		return "AstMetaKindExpression"
	case AstMetaKindBlock:
		return "AstMetaKindBlock"
	case AstMetaKindParenthesis:
		return "AstMetaKindParenthesis"
	case AstMetaKindList:
		return "AstMetaKindList"
	case AstMetaKindCapture:
		return "AstMetaKindCapture"
	case AstMetaKindWordValue:
		return "AstMetaKindWordValue"
	case AstMetaKindWord:
		return "AstMetaKindWord"
	case AstMetaKindType:
		return "AstMetaKindType"
	case AstMetaKindValue:
		return "AstMetaKindValue"
	case AstMetaKindEnd:
		return "AstMetaKindEnd"
	case AstMetaKindError:
		return "AstMetaKindError"
	default:
		return "Unknown AstMetaKind"
	}
}
*/

/*
func AstAppendChild(parent Ast, child Ast) Ast {
	basicParent := parent.(*Ast)
	return basicParent.AppendChild(child)
}

func AstNewChild(parent Ast, kind AstMetaKind, token Token) Ast {
	basicParent := parent.(*Ast)
	return basicParent.NewChild(kind, token)
}
*/

type basicAst struct {
	parent   *AstNode
	children []*AstNode
	token    Token
}

type AstNode interface {
    Parent() AstNode
    Children() []AstNode
}


/* AST node kind */
type Ast struct {
	AstKind
	parent   *Ast
	children []*Ast
	token    Token
}

func (ast Ast) Value() Value {
	return ast.token.Value
}

func NewAst(kind AstKind, parent *Ast, children []*Ast, token Token) *Ast {
	ast := &Ast{AstKind: kind, parent: parent, token: token}
    return ast.AppendChildren(children...)
}

func (ast *Ast) AppendChildren(children ...*Ast) *Ast {
	for _, child := range children {
        ast.AppendChild(child)
    }
    return ast
}


func (ast *Ast) AppendChild(child * Ast) *Ast {
	child.SetParent(ast)
    ast.children = append(ast.children, child)
	return child
}

func (ast *Ast) NewChild(kind AstKind, token Token) *Ast {
	child := NewAst(kind, ast, make([]*Ast, 0), token)
	ast.AppendChild(child)
	return child
}

func (ast Ast) IsKind(astkind AstKind) bool {
	return ast.AstKind == astkind
}

func (ast Ast) IsError() bool {
	return ast.AstKind == AstKindError
}

func (ast Ast) IsNone() bool {
	return ast.AstKind == AstKindNone
}

func (ast Ast) IsFlatten() bool {
	return ast.AstKind == AstKindFlatten
}

func (ast Ast) Token() Token {
	return ast.token
}

func (ast Ast) Parent() *Ast {
	return ast.parent
}

func (ast Ast) Children() []*Ast {
	return ast.children
}

func (ast Ast) Kind() AstKind {
	return ast.AstKind
}

func (ast *Ast) SetParent(parent *Ast) {
	ast.parent = parent
}

func (ast Ast) Child(index int) *Ast {
	count := ast.CountChildren()
	if index < 0 || index > count {
		return nil
	}
	return ast.children[index]
}


func (ast *Ast) Walk(walker func(node *Ast) *Ast) *Ast {
	if found := walker(ast); found != nil {
		return found
	}
	for _, child := range ast.children {
		if found := child.Walk(walker); found != nil {
			return found
		}
	}
	return nil
}

func (ast Ast) String() string {
	return fmt.Sprintf("Ast %s: %s", ast.AstKind.String(), ast.token.String())
}

func (ast Ast) Display() {
	ast.Walk(func(node *Ast) *Ast {
		depth := node.Depth()
		fmt.Printf("%s", strings.Repeat("--", depth))
		if node != nil {
			fmt.Printf("Ast: %s\n", node.String())
		} else {
			fmt.Printf("Ast: nil node\n")
		}
		return nil
	})
}

func (ast Ast) Dump() string {
	result := ""
	ast.Walk(func(node *Ast) *Ast {
		depth := node.Depth()
		result += fmt.Sprintf("%s", strings.Repeat("--", depth))
		if node != nil {
			result += fmt.Sprintf("Ast: %s\n", node.String())
		} else {
			result += fmt.Sprintf("Ast: nil node\n")
		}
		return nil
	})
	return result
}


func (ast Ast) Depth() int {
	var depth int = 0
	parent := ast.Parent()
	for parent != nil {
		depth++
		parent = parent.Parent()
	}
	return depth
}

func (ast Ast) CountChildren() int {
	return len(ast.children)
}

func AstIsError(ast * Ast) bool {
	return ast.IsError()
}

func (ast Ast) Errors() []*Ast {
	res := make([]*Ast, 0)
	ast.Walk(func(node *Ast) *Ast {
		if node != nil && ast.IsError() {
			res = append(res, node)
		}
		return nil
	})
	return res
}

func EmptyAstArray() []*Ast {
	return make([]*Ast, 0)
}

func NewEmptyAst(astkind AstKind) *Ast {
	return NewAst(astkind, nil, EmptyAstArray(), NoToken())
}

func NewAstNone() *Ast {
	return NewEmptyAst(AstKindNone)
}

func NewAstWithToken(astkind AstKind, token Token) *Ast {
	return NewAst(astkind, nil, EmptyAstArray(), token)
}

// If AST has errors, return it as a merged error, otherwise returns nil 
func (ast *Ast) ToError() error {
	errlist := ast.Errors()
	if len(errlist) < 1 {
		return nil
	} 
	sep := ""
	res := ""
	for _, err := range errlist {
		res = fmt.Sprintf("%s%s%s", res, sep, err)
		sep = "\n"
	}
	return fmt.Errorf("%s", res)
}

func (from Ast) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *Ast:
			(*toPtr) = from
		default:
			return NewErrorValuef("Cannot convert Ast value %v to %v", from, to)
	}
	return nil
}

type TypeValue string

const TypeValueAst = TypeValue("Ast")

func (ast Ast) Type() TypeValue {
	return TypeValueAst
}



/*
type AstProgram struct{ Ast }
type AstStatements struct{ Ast }
type AstStatement struct{ Ast }
type AstSet struct{ Ast }
type AstGet struct{ Ast }
type AstTarget struct{ Ast }
type AstCommand struct{ Ast }
type AstArguments struct{ Ast }
type AstArgument struct{ Ast }
type AstExpression struct{ Ast }
type AstBlock struct{ Ast }
type AstParenthesis struct{ Ast }
type AstList struct{ Ast }
type AstCapture struct{ Ast }
type AstWordValue struct{ Ast }
type AstWord struct{ Ast }
type AstType struct{ Ast }
type AstValue struct{ Ast }
type AstEnd struct{ Ast }
type AstError struct{ Ast }
*/

var _ Ast = Ast{}

/* The kind of an AST is also it's constructor function. This is so deep I'm almost
getting scared. :) */
// The type is defined as a constructor

/*
type AstMetaKind func(parent Ast, children []Ast, token Token) Ast

type AstMetaKind2 interface {
	New(parent Ast, children []Ast, token Token) Ast
}

type AstTypeProgramImp struct{}

func (ap AstTypeProgramImp) New(parent Ast, children []Ast, token Token) Ast {
	return AstProgram{NewBasicAst(parent, children, token)}
}

func NewAstProgram(parent Ast, children []Ast, token Token) Ast {
	return AstProgram{NewBasicAst(parent, children, token)}
}

var AstMetaKindBasic = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast { return NewBasicAst(parent, children, token) })

var AstMetaKindProgram = AstMetaKind(NewAstProgram)
var AstMetaKindProgram2 = AstMetaKind2(AstTypeProgramImp{})

var AstMetaKindStatements = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstStatements{NewBasicAst(parent, children, token)}
})

var AstMetaKindStatement = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstStatement{NewBasicAst(parent, children, token)}
})
var AstMetaKindSet = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstSet{NewBasicAst(parent, children, token)}
})
var AstMetaKindGet = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstGet{NewBasicAst(parent, children, token)}
})
var AstMetaKindTarget = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstTarget{NewBasicAst(parent, children, token)}
})
var AstMetaKindCommand = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstCommand{NewBasicAst(parent, children, token)}
})
var AstMetaKindArguments = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstArguments{NewBasicAst(parent, children, token)}
})
var AstMetaKindArgument = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstArgument{NewBasicAst(parent, children, token)}
})
var AstMetaKindExpression = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstExpression{NewBasicAst(parent, children, token)}
})
var AstMetaKindBlock = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstBlock{NewBasicAst(parent, children, token)}
})
var AstMetaKindParenthesis = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstParenthesis{NewBasicAst(parent, children, token)}
})
var AstMetaKindList = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstList{NewBasicAst(parent, children, token)}
})
var AstMetaKindCapture = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstCapture{NewBasicAst(parent, children, token)}
})
var AstMetaKindWordValue = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstWordValue{NewBasicAst(parent, children, token)}
})
var AstMetaKindWord = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstWord{NewBasicAst(parent, children, token)}
})
var AstMetaKindType = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstType{NewBasicAst(parent, children, token)}
})
var AstMetaKindValue = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstValue{NewBasicAst(parent, children, token)}
})
var AstMetaKindEnd = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstEnd{NewBasicAst(parent, children, token)}
})
var AstMetaKindError = AstMetaKind(func(parent Ast, children []Ast, token Token) Ast {
	return &AstError{NewBasicAst(parent, children, token)}
})
*/
