package parser

// SizeAlign if a helper to embed in types that have a size and alignment
type SizeAlign struct {
    Size uint
    Align uint
}

// A type models a built in or defined Beo type.
type Type interface {
    // Get the common type information.
    Info() TypeInfo
}

// TypeInfo contains the common information about a Type.
type TypeInfo struct {    
    // Type has an idenitifier
    identifier
    // Size and alignment
    SizeAlign
    // Type parameters for parametric polymorphic types, empty if not 
    // polymorphic
    Parameters []Type
    // Methods defined on the type
    Methods []Method
}

// Implement the Identifier interface
var _ Identifier = TypeInfo{}

func MakeTypeInfo(pack *Package, name string, size uint, align uint, parameters []Type, methods []Method) TypeInfo {
    return TypeInfo {
        identifier: identifier {
            name: name,
            pack: pack,
        }, 
        SizeAlign : SizeAlign {
            Size: size,
            Align: align,
        },
        Parameters: parameters,
        Methods: methods,
    }
}

func (ti TypeInfo) Info() TypeInfo {
    return ti
}

type AliasType struct {
    TypeInfo
    Alias Type
}

type InterfaceType struct {
    TypeInfo
    // Methods required on the type that implements the interface
    Methods []Method
    // Embedded members, separate since they are treated differently
    Embedded []InterfaceType
    // Type list for interfaces that are sum types, empty
    // if not a sum type    
    SumOf []Type
}

type StructType struct {
    TypeInfo
    // Members of the struct with their types
    Members []Type
    // Embedded members, separate since they are treated differently
    Embedded []Type
    SizeAlign
}

// array is a sequential collection of items of the same type, of which 
// the length is known at compile time. 
type ArrayType struct {
    TypeInfo
    SizeAlign
    Length uint
    // type of the members of the array
    Member Type
}

// slice is a sequential collection of items of the same type, of which 
// the length is known only at run time. 
type SliceType struct {
    TypeInfo
    // Here the size and alignment is that of the slice header itself
    // not of the members
    SizeAlign
    // type of the members of the slice
    Member Type
}


// map is a keyed collection of items of the same type 
type MapType struct {
    TypeInfo
    // Here the size and alignment is that of the map header itself
    // not of the members
    SizeAlign
    // type of the members of the map
    Member Type
    // typetype of the keys of the map
    Key Type
}

// A function is a callable block with a name
type Function struct {
    // A function has an identifier
    identifier
    // Size of the type in bytes
    Size uint
    // Alignment requirement of the type
    Align uint
    // Type of the functions arguments
    Arguments []Type
    // Type of the functions return values
    Returns []Type        
    // Type parameters for parametric polymorphic types, empty if not
    // polymorphic
    Parameters []Type
    // Body is the block that implements the body body of the function
    Body Block
}

type Block struct {
    Statements []Statement
}

func (Block) isStatement() {
}

type Statement interface {
    isStatement()
}

type statement struct {
}

func (statement) isExpression() {
}

type Expression interface {
    isExpression()
}

type expression struct {
}

func (expression) isExpression() {
}


type Condition struct {
    Expression []Expression
}

type IfStatement struct {
    Cond Condition
    Then Block
    Else Block
    statement
}

type CaseStatement struct {
    Cond Condition
    Body Block
    statement
} 

type SwitchStatement struct {
    Cond Condition
    Cases []CaseStatement
    Default Block
    statement
}

type ForStatement struct {
    Cond Condition
    Body Block
    statement
}

type ReturnStatement struct {
    Body Expression
    statement
}

type Label struct {
    Name string
    Position    
}

type BreakStatement struct {
    *Label
    statement
}

type ContinueStatement struct {
    *Label
    statement
}

type FallthroughStatement struct {
    From *CaseStatement
    To *CaseStatement
    statement
}

type GotoStatement struct {
    *Label
    statement
}

type RangeExpression struct {
    Of *Expression
    expression
}

type DeferStatement struct {
    What *Expression
    statement
}

type SelectStatement struct {
    Cond Condition
    Cases []CaseStatement
    Default Block
    statement
}

type VarStatement struct {
    Names []string
    Type
    Initializers []Expression
    statement
}

// A method is a function that takes a type as its first, special argument.
type Method struct {
    // the name of the method
    name string
    // type the method belongs to
    Type
    // this is a special case of a function
    Function
}
    
    
const (
    // TypeType is the type of TypeValue itself.
    TypeType   		= TypeValue("Type")
	// AnyType is not really a type, but a wildcard that matches any type
	AnyType    		= TypeValue("Any")
	// ZeroType in not really a type, but means that the type is missing, 
	// and is conventiently the zero value for TypeValue
	ZeroType   		= TypeValue("")
)



func (val TypeValue) String() string {
	return string(val)
}

func (v TypeValue) Type() TypeValue   { return TypeType }

var _ Value = TypeValue("")


func (from TypeValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *TypeValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from
		default:
			return NewErrorValuef("Cannot convert value %v to %v", from, to)
	}
	return nil
}



