package parser

import "fmt"

// IntValue are Muesli integer values
type IntValue int64
// IntType is the type of IntValue
const IntType    		= TypeValue("Int")

func (val IntValue) String() string {
	return fmt.Sprintf("%d", int64(val))
}

func (v IntValue) Type() TypeValue    { return IntType }

func (from IntValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *int8:
			(*toPtr) = int8(from)
		case *int16:
			(*toPtr) = int16(from)
		case *int32:
			(*toPtr) = int32(from)
		case *int64:
			(*toPtr) = int64(from)
		case *int:
			(*toPtr) = int(from)
		case *bool:			 
			(*toPtr) = (from != 0)
		case *float32:
			(*toPtr) = float32(from)
		case *float64:
			(*toPtr) = float64(from)
		case *IntValue:
			(*toPtr) = from
		case *Value:
			(*toPtr) = from			
		default:
			return NewErrorValuef("Cannot convert IntValue value %v to %v", from, to)
	}
	return nil
}

