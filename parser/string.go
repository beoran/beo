package parser

// StringValue are Muesli strings
type StringValue string

const StringType 		= TypeValue("String")

func (val StringValue) String() string {
	return string(val)
}

func (from StringValue) Convert(to interface{}) error {
	switch toPtr := to.(type) {
		case *string:
			(*toPtr) = from.String()
		case *StringValue:
			(*toPtr) = from	
		case *Value:
			(*toPtr) = from	
		default:
			return NewErrorValuef("Cannot convert StringValue %v to %v", from, to)
	}
	return nil
}

func (v StringValue) Type() TypeValue { return StringType }


